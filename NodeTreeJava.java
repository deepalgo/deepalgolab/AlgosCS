public class NodeTreeJava
{
    public NodeTreeJava(int value, NodeTreeJava left, NodeTreeJava right)
    {
	Value = value;
	Right = right;
	Left = left;
    }

    public int Value;
    public NodeTreeJava Right;
    public NodeTreeJava Left;

    //@deepalgo
    public static NodeTreeJava InsertValueJava(NodeTreeJava root, int value)
    {
	if (root == null)
	{
	    return new NodeTreeJava(value, null, null);
	}
	if (value < root.Value)
	{
	    root.Left = InsertValueJava(root.Left, value);
	}
	else
	{
	    root.Right = InsertValueJava(root.Right, value);
	}
	return root;
    }
}
